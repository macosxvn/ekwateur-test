<?php

/**
 * Project ekwateur
 * @author Louis Nguyen <nvt2302@gmail.com>
 * Date 07/12/2021
 */

namespace App\Service;

use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Contracts\HttpClient\Exception\ClientExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\RedirectionExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\ServerExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\TransportExceptionInterface;
use Symfony\Contracts\HttpClient\HttpClientInterface;

class ApiClientService
{

    /** @var HttpClientInterface */
    private HttpClientInterface $httpClient;

    /** @var string */
    private string $baseUrl;

    /**
     * @param HttpClientInterface $httpClient
     * @param string $baseUrl
     */
    public function __construct(HttpClientInterface $httpClient, string $baseUrl)
    {
        $this->httpClient = $httpClient;
        $this->baseUrl = $baseUrl;
    }

    /**
     * @param string $endpoint
     * @param array<string, string> $query
     * @return string
     */
    public function fetch(string $endpoint, array $query = []): string
    {
        $url = $this->baseUrl . $endpoint;
        try {
            $response = $this->httpClient->request('GET', $url, ['query' => $query]);
            if ($response->getStatusCode() === 200) {
                return $response->getContent();
            }
        } catch (
            TransportExceptionInterface |
            ClientExceptionInterface |
            RedirectionExceptionInterface |
            ServerExceptionInterface $e
        ) {
            // TODO: log api error
        }

        // In all case of error from api, return json of empty array
        return '[]';
    }
}
