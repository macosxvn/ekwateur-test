<?php

/**
 * Project ekwateur
 * @author Louis Nguyen <nvt2302@gmail.com>
 * Date 08/12/2021
 */

namespace App\Service;

use App\Model\PromoCode;
use App\Repository\OfferRepository;
use App\Repository\PromoCodeRepository;
use Symfony\Component\Filesystem\Filesystem;
use Symfony\Component\Filesystem\Path;

class PromoCodeService
{
    private PromoCodeRepository $promoCodeRepository;
    private OfferRepository $offerRepository;
    private string $outputDir;
    private Filesystem $filesystem;

    /**
     * @param PromoCodeRepository $promoCodeRepository
     * @param OfferRepository $offerRepository
     * @param string $outputDir
     */
    public function __construct(
        PromoCodeRepository $promoCodeRepository,
        OfferRepository $offerRepository,
        string $outputDir
    ) {
        $this->promoCodeRepository = $promoCodeRepository;
        $this->offerRepository = $offerRepository;
        $this->outputDir = Path::normalize($outputDir);
        $this->filesystem = new Filesystem();
    }

    /**
     * @param string $code
     * @return PromoCode|null
     */
    public function findByCode(string $code): ?PromoCode
    {
        $promoCode = $this->promoCodeRepository->findOneBy(['code' => $code]);
        // Promo code exist & not expired - load the offer list of this promotion code
        if ($promoCode && !$this->isExpired($promoCode)) {
            $this->loadOffers($promoCode);
        }

        return $promoCode;
    }

    public function isValid(PromoCode $promoCode): bool
    {
        return !$this->isExpired($promoCode) && count($promoCode->getCompatibleOfferList());
    }

    public function isExpired(PromoCode $promoCode): bool
    {
        $endDate = strtotime($promoCode->getEndDate() . ' 23:59:59');
        // end date format is wrong or expired
        return ($endDate === false || $endDate < time());
    }

    public function loadOffers(PromoCode $promoCode): void
    {
        $offers = $this->offerRepository->findBy(['validPromoCodeList' => $promoCode->getCode()]);
        $promoCode->setCompatibleOfferList($offers);
    }

    /**
     * TODO: Find the way to use symfony serializer with naming convert
     * @param PromoCode $promoCode
     * @param bool $prettyPrint
     * @return string
     */
    public function toJson(PromoCode $promoCode, bool $prettyPrint = true): string
    {
        $jsonData = [
            'promoCode' => $promoCode->getCode(),
            'endDate' => $promoCode->getEndDate(),
            'discountValue' => $promoCode->getDiscountValue(),
        ];
        foreach ($promoCode->getCompatibleOfferList() as $offer) {
            $jsonData['compatibleOfferList'][] = [
                'name' => $offer->getOfferName(),
                'type' => $offer->getOfferType(),
            ];
        }

        return (string)json_encode($jsonData, $prettyPrint ? JSON_PRETTY_PRINT : 0);
    }

    public function toFile(PromoCode $promoCode, string $filename = '', bool $prettyPrint = true): void
    {
        if (empty($filename)) {
            $filename = strtolower($promoCode->getCode()) . '.json';
        }

        if (!$this->filesystem->exists($this->outputDir)) {
            $this->filesystem->mkdir($this->outputDir);
        }

        $json = $this->toJson($promoCode, $prettyPrint);
        $this->filesystem->dumpFile($this->outputDir . '/' . $filename, $json);
    }
}
