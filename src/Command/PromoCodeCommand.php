<?php

/**
 * Project ekwateur
 * @author Louis Nguyen <nvt2302@gmail.com>
 * Date 08/12/2021
 */

namespace App\Command;

use App\Service\PromoCodeService;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

class PromoCodeCommand extends Command
{
    protected static $defaultName = 'promo-code:validate';
    private string $fileName = '';
    private bool $outputConsole = false;
    private bool $prettyPrint = true;
    private PromoCodeService $promoCodeService;
    private string $outputDir;

    /**
     * @param PromoCodeService $promoCodeService
     * @param string $outputDir
     */
    public function __construct(PromoCodeService $promoCodeService, string $outputDir)
    {
        parent::__construct();
        $this->promoCodeService = $promoCodeService;
        $this->outputDir = $outputDir;
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $this->analyseInput($input);
        // Upper case the input promotion code to avoid case-sensitive problem
        $code = strtoupper($input->getArgument('promoCode'));
        $promoCode = $this->promoCodeService->findByCode($code);
        // Promo code is valid & have the offers related
        if ($promoCode && !empty($promoCode->getCompatibleOfferList())) {
            if ($this->outputConsole) {
                $json = $this->promoCodeService->toJson($promoCode, $this->prettyPrint);
                $output->write($json);
            } else {
                $this->promoCodeService->toFile($promoCode, $this->fileName, $this->prettyPrint);
            }
        }
        return Command::SUCCESS;
    }

    protected function configure(): void
    {
        $this
            ->setDescription('The command line to validate the given promotion code')
            ->addArgument('promoCode', InputArgument::REQUIRED, 'The promotion code need to validate')
            ->addOption('pretty', 'p', InputOption::VALUE_OPTIONAL, 'Pretty print json object in output file', true)
            ->addOption('output', 'o', InputOption::VALUE_OPTIONAL, 'Output to console', false)
            ->addOption('file', 'f', InputOption::VALUE_OPTIONAL, 'Output file name', '')
        ;
    }

    /**
     * TODO: fix the conflict with command tester
     * Override the output interface to write to file instead of write to console
     * @param OutputInterface $output
     * @throws \ReflectionException
     */
    protected function overrideOutput(OutputInterface $output): void
    {
        $reflectedOutput = new \ReflectionObject($output);
        $reflectedParent = $reflectedOutput->getParentClass();
        if ($reflectedParent === false) {
            return;
        }
        $streamProperty = $reflectedParent->getProperty('stream');
        $streamProperty->setAccessible(true);
        $streamProperty->setValue($output, fopen($this->outputDir . '/' . $this->fileName, 'w', false));
    }

    protected function analyseInput(InputInterface $input): void
    {
        $this->fileName = $input->getOption('file');
        $this->outputConsole = $input->getOption('output');
        $this->prettyPrint = filter_var($input->getOption('pretty'), FILTER_VALIDATE_BOOLEAN);
    }
}
