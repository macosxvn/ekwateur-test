<?php

/**
 * Project ekwateur
 * @author Louis Nguyen <nvt2302@gmail.com>
 * Date 08/12/2021
 */

namespace App\Repository;

use App\Model\PromoCode;
use App\Service\ApiClientService;
use Symfony\Component\Serializer\Encoder\JsonEncoder;
use Symfony\Component\Serializer\Normalizer\ArrayDenormalizer;
use Symfony\Component\Serializer\Normalizer\GetSetMethodNormalizer;
use Symfony\Component\Serializer\Serializer;

class PromoCodeRepository
{
    private Serializer $serializer;
    private ApiClientService $apiClientService;

    /**
     * @param ApiClientService $apiClientService
     */
    public function __construct(ApiClientService $apiClientService)
    {
        $this->apiClientService = $apiClientService;
        $this->serializer = new Serializer(
            [new GetSetMethodNormalizer(), new ArrayDenormalizer()],
            [new JsonEncoder()]
        );
    }

    /**
     * @param array<string, string> $filters
     * @return array<int, PromoCode>
     */
    public function findBy(array $filters = []): array
    {
        $apiData = $this->apiClientService->fetch('/promoCodeList', $filters);
        return $this->serializer->deserialize($apiData, 'App\Model\PromoCode[]', 'json');
    }


    /**
     * @param array<string, string> $filters
     * @return PromoCode|null
     */
    public function findOneBy(array $filters): ?PromoCode
    {
        $filters = array_merge($filters, ['page' => '1', 'limit' => '1']);
        $response = $this->findBy($filters);
        if (!empty($response)) {
            return $response[0];
        } else {
            return null;
        }
    }
}
