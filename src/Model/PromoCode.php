<?php

/**
 * Project ekwateur
 * @author Louis Nguyen <nvt2302@gmail.com>
 * Date 08/12/2021
 */

namespace App\Model;

class PromoCode
{
    private string $code;
    private float $discountValue;
    private string $endDate;
    /** @var array<int, Offer> */
    private array $compatibleOfferList = [];

    /**
     * @return string
     */
    public function getCode(): string
    {
        return $this->code;
    }

    /**
     * @param string $code
     */
    public function setCode(string $code): void
    {
        $this->code = $code;
    }

    /**
     * @return float
     */
    public function getDiscountValue(): float
    {
        return $this->discountValue;
    }

    /**
     * @param float $discountValue
     */
    public function setDiscountValue(float $discountValue): void
    {
        $this->discountValue = $discountValue;
    }

    /**
     * @return string
     */
    public function getEndDate(): string
    {
        return $this->endDate;
    }

    /**
     * @param string $endDate
     */
    public function setEndDate(string $endDate): void
    {
        $this->endDate = $endDate;
    }

    /**
     * @return array<int, Offer>
     */
    public function getCompatibleOfferList(): array
    {
        return $this->compatibleOfferList;
    }

    /**
     * @param array<int, Offer> $compatibleOfferList
     */
    public function setCompatibleOfferList(array $compatibleOfferList): void
    {
        $this->compatibleOfferList = $compatibleOfferList;
    }
}
