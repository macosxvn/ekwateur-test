<?php

/**
 * Project ekwateur
 * @author Louis Nguyen <nvt2302@gmail.com>
 * Date 08/12/2021
 */

namespace App\Model;

class Offer
{
    private string $offerType;
    private string $offerName;
    private string $offerDescription;
    /** @var array<int, string> */
    private array $validPromoCodeList;

    /**
     * @return string
     */
    public function getOfferType(): string
    {
        return $this->offerType;
    }

    /**
     * @param string $offerType
     */
    public function setOfferType(string $offerType): void
    {
        $this->offerType = $offerType;
    }

    /**
     * @return string
     */
    public function getOfferName(): string
    {
        return $this->offerName;
    }

    /**
     * @param string $offerName
     */
    public function setOfferName(string $offerName): void
    {
        $this->offerName = $offerName;
    }

    /**
     * @return string
     */
    public function getOfferDescription(): string
    {
        return $this->offerDescription;
    }

    /**
     * @param string $offerDescription
     */
    public function setOfferDescription(string $offerDescription): void
    {
        $this->offerDescription = $offerDescription;
    }

    /**
     * @return array<int, string>
     */
    public function getValidPromoCodeList(): array
    {
        return $this->validPromoCodeList;
    }

    /**
     * @param array<int, string> $validPromoCodeList
     */
    public function setValidPromoCodeList(array $validPromoCodeList): void
    {
        $this->validPromoCodeList = $validPromoCodeList;
    }
}
