### Overview
The command line verify the validation of a promotion code and display its offers. The output of command line by default is saved in a json file. An option is provided to display the output directly on the console.

### Usage

```
bin/console promo-code:validate [options] [--] <promoCode>

Arguments:
  promoCode              The promotion code need to validate

Options:
  -p, --pretty[=PRETTY]  Pretty print json object in output file [default: true]
  -o, --output[=OUTPUT]  Output to console [default: false]
  -f, --file[=FILE]      Output file name [default: ""]
```

### Example

#### 1. Default
```
bin/console promo-code:validate ELEC_N_WOOD
```
The json output file (`elect_n_wood.json`) is stored in `output` folder

The content:
```json
{
    "promoCode": "ELEC_N_WOOD",
    "endDate": "2022-06-20",
    "discountValue": 1.5,
    "compatibleOfferList": [
        {
            "name": "EKWAE3000",
            "type": "ELECTRICITY"
        },
        {
            "name": "EKWAW3000",
            "type": "WOOD"
        }
    ]
}
```

#### 2. Output to console

```
bin/console promo-code:validate ELEC_N_WOOD --output=true
```

The json output is displayed directly on console


#### 3. Disable json pretty print

```
bin/console promo-code:validate ELEC_N_WOOD --pretty=false
```

The json output file (`elect_n_wood.json`) is stored in `output` folder

The content:
```json
{"promoCode":"ELEC_N_WOOD","endDate":"2022-06-20","discountValue":1.5,"compatibleOfferList":[{"name":"EKWAE3000","type":"ELECTRICITY"},{"name":"EKWAW3000","type":"WOOD"}]}
```

#### 4. Custom output file name
```
bin/console promo-code:validate ELEC_N_WOOD --file=custom_name.json
```
The json output file (`custom_name.json`) is stored in `output` folder