<?php

/**
 * Project ekwateur
 * @author Louis Nguyen <nvt2302@gmail.com>
 * Date 08/12/2021
 */

namespace App\Tests\Command;

use Symfony\Bundle\FrameworkBundle\Console\Application;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;
use Symfony\Component\Console\Tester\CommandTester;

class PromoCodeCommandTest extends KernelTestCase
{
    private CommandTester $commandTester;
    private string $outputDir;

    protected function setUp(): void
    {
        $kernel = self::bootKernel();
        $application = new Application($kernel);
        $command = $application->find('promo-code:validate');
        $this->commandTester = new CommandTester($command);
        $this->outputDir = $kernel->getContainer()->getParameter('json_output');
        sleep(1);
    }

    public function testPromoCodeValid(): void
    {
        $this->commandTester->execute(
            [
                'promoCode' => 'ELEC_N_WOOD',
                '--pretty' => false, // disable pretty print
                '--output' => true, // display output to console
            ]
        );

        $this->commandTester->assertCommandIsSuccessful();
        $output = $this->commandTester->getDisplay();
        $expectedOutput = file_get_contents(__DIR__ . '/../Fixture/elec_n_wood.json');
        $this->assertEquals($expectedOutput, $output);
    }

    public function testPromoCodeInvalid(): void
    {
        $this->commandTester->execute(
            [
                'promoCode' => 'ELEC_N_WOOD_INVALID',
                '--pretty' => false, // disable pretty print
                '--output' => true, // display output to console
            ]
        );

        $this->commandTester->assertCommandIsSuccessful();
        $output = $this->commandTester->getDisplay();
        $this->assertEquals('', $output);
        $this->assertEquals('output', $this->outputDir);
    }

    public function testOutputToFile(): void
    {
        $code = 'ELEC_N_WOOD';
        $this->commandTester->execute(
            [
                'promoCode' => $code,
            ]
        );

        $this->commandTester->assertCommandIsSuccessful();
        // By default, json file name should be promo code in lower case
        $filePath = __DIR__ . '/../' . $this->outputDir . '/' . strtolower($code) . '.json';
        $this->assertTrue(file_exists($filePath));
        $output = file_get_contents($filePath);
        $expectedOutput = file_get_contents(__DIR__ . '/../Fixture/elec_n_wood_pretty.json');
        $this->assertEquals($expectedOutput, $output);
    }

    public function testOutputToCustomFileName(): void
    {
        $this->commandTester->execute(
            [
                'promoCode' => 'ELEC_N_WOOD',
                '--file' => 'custom_name.json'
            ]
        );

        $this->commandTester->assertCommandIsSuccessful();
        // By default, json file name should be promo code in lower case
        $filePath = __DIR__ . '/../' . $this->outputDir . '/custom_name.json';
        $this->assertTrue(file_exists($filePath));
        $output = file_get_contents($filePath);
        $expectedOutput = file_get_contents(__DIR__ . '/../Fixture/elec_n_wood_pretty.json');
        $this->assertEquals($expectedOutput, $output);
    }
}
