<?php

/**
 * Project ekwateur
 * @author Louis Nguyen <nvt2302@gmail.com>
 * Date 08/12/2021
 */

namespace App\Tests\Repository;

use App\Repository\PromoCodeRepository;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;

class PromoCodeRepositoryTest extends KernelTestCase
{
    private PromoCodeRepository $promoCodeRepository;

    public function setUp(): void
    {
        self::bootKernel();
        $container = static::getContainer();
        $this->promoCodeRepository = $container->get(PromoCodeRepository::class);
        sleep(1);
    }

    public function testFindAll(): void
    {
        $promoCodes = $this->promoCodeRepository->findBy();
        // Have 8 promotion code in test API
        $this->assertEquals(8, count($promoCodes));
    }

    public function testFindBy(): void
    {
        $welcomePromos = $this->promoCodeRepository->findBy(['code' => 'EKWA_WELCOME']);
        $this->assertEquals(1, count($welcomePromos));
        $welcomePromo = $welcomePromos[0];
        $this->assertEquals('EKWA_WELCOME', $welcomePromo->getCode());
        $this->assertEquals(2, $welcomePromo->getDiscountValue());
        $this->assertEquals('2019-10-04', $welcomePromo->getEndDate());
    }

    public function testFindOneBy(): void
    {
        $welcomePromo = $this->promoCodeRepository->findOneBy(['code' => 'EKWA_WELCOME']);
        $this->assertNotNull($welcomePromo);
        $this->assertEquals('EKWA_WELCOME', $welcomePromo->getCode());
        $this->assertEquals(2, $welcomePromo->getDiscountValue());
        $this->assertEquals('2019-10-04', $welcomePromo->getEndDate());
    }

    public function testNotFound(): void
    {
        $welcomePromo = $this->promoCodeRepository->findOneBy(['code' => 'EKWA_WELCOME_NOT_FOUND']);
        $this->assertNull($welcomePromo);

        $welcomePromos = $this->promoCodeRepository->findBy(['code' => 'EKWA_WELCOME_NOT_FOUND']);
        $this->assertEmpty($welcomePromos);
    }
}
