<?php

/**
 * Project ekwateur
 * @author Louis Nguyen <nvt2302@gmail.com>
 * Date 08/12/2021
 */

namespace App\Tests\Repository;

use App\Repository\OfferRepository;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;

class OfferRepositoryTest extends KernelTestCase
{
    private OfferRepository $offerRepository;

    public function setUp(): void
    {
        self::bootKernel();
        $container = static::getContainer();
        $this->offerRepository = $container->get(OfferRepository::class);
        sleep(1);
    }

    public function testFindAll(): void
    {
        $offers = $this->offerRepository->findBy();
        // Have 8 promotion code in test API
        $this->assertEquals(6, count($offers));
    }

    public function testFindBy(): void
    {
        $gasOffers = $this->offerRepository->findBy(['offerType' => 'GAS']);
        $this->assertEquals(2, count($gasOffers));
        $offer1 = $gasOffers[0];
        $this->assertEquals('GAS', $offer1->getOfferType());
        $this->assertEquals('EKWAG2000', $offer1->getOfferName());
        $this->assertEquals('Une offre incroyable', $offer1->getOfferDescription());
    }

    public function testFindOneBy(): void
    {
        $gasOffer = $this->offerRepository->findOneBy(['offerType' => 'GAS']);
        $this->assertNotNull($gasOffer);
        $this->assertEquals('GAS', $gasOffer->getOfferType());
        $this->assertEquals('EKWAG2000', $gasOffer->getOfferName());
        $this->assertEquals('Une offre incroyable', $gasOffer->getOfferDescription());
    }

    public function testNotFound(): void
    {
        $offer = $this->offerRepository->findOneBy(['offerType' => 'UNKNOWN_TYPE']);
        $this->assertNull($offer);

        $offers = $this->offerRepository->findBy(['offerType' => 'UNKNOWN_TYPE']);
        $this->assertEmpty($offers);
    }
}
