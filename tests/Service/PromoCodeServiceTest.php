<?php

/**
 * Project ekwateur
 * @author Louis Nguyen <nvt2302@gmail.com>
 * Date 08/12/2021
 */

namespace App\Tests\Service;

use App\Service\PromoCodeService;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;

class PromoCodeServiceTest extends KernelTestCase
{
    private PromoCodeService $promoCodeService;

    public function setUp(): void
    {
        self::bootKernel();
        $container = static::getContainer();
        $this->promoCodeService = $container->get(PromoCodeService::class);
        sleep(1);
    }

    public function testValidPromoCode(): void
    {
        $promoCode = $this->promoCodeService->findByCode('ELEC_N_WOOD');
        $this->assertInstanceOf('App\Model\PromoCode', $promoCode);
        $this->assertEquals(2, count($promoCode->getCompatibleOfferList()));
        $expectedOutput = file_get_contents(__DIR__ . '/../Fixture/elec_n_wood_pretty.json');
        $jsonPrettyOutput = $this->promoCodeService->toJson($promoCode);
        $this->assertEquals($expectedOutput, $jsonPrettyOutput);

        $expectedOutput = file_get_contents(__DIR__ . '/../Fixture/elec_n_wood.json');
        $jsonOutput = $this->promoCodeService->toJson($promoCode, false);
        $this->assertEquals($expectedOutput, $jsonOutput);
    }

    public function testExpiredPromoCode(): void
    {
        $promoCode = $this->promoCodeService->findByCode('EKWA_WELCOME');
        $this->assertInstanceOf('App\Model\PromoCode', $promoCode);
        $this->assertTrue($this->promoCodeService->isExpired($promoCode));
        $this->assertEmpty($promoCode->getCompatibleOfferList());
    }
}
