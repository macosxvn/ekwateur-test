<?php

/**
 * Project ekwateur
 * @author Louis Nguyen <nvt2302@gmail.com>
 * Date 07/12/2021
 */

namespace App\Tests\Service;

use App\Service\ApiClientService;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class ApiClientServiceTest extends KernelTestCase
{
    private ApiClientService $apiClientService;
    public function setUp(): void
    {
        self::bootKernel();
        $container = static::getContainer();
        $this->apiClientService = $container->get(ApiClientService::class);
        sleep(1);
    }

    public function testPromoCodeList(): void
    {
        $list = json_decode($this->apiClientService->fetch('/promoCodeList'), true);
        // have 8 promo code list
        $this->assertEquals(8, count($list));
    }

    public function testPromoCodeWelcome(): void
    {
        $list = json_decode($this->apiClientService->fetch('/promoCodeList', ['code' => 'EKWA_WELCOME']), true);
        // have 1 promo code EKWA_WELCOME
        $this->assertEquals(1, count($list));
        $this->assertEquals(
            [
                'code' => 'EKWA_WELCOME',
                'discountValue' => 2,
                'endDate' => '2019-10-04'
            ],
            $list[0]
        );
    }

    public function testOfferList(): void
    {
        $list = json_decode($this->apiClientService->fetch('/offerList'), true);
        // have 6 offers
        $this->assertEquals(6, count($list));
    }

    public function testGasOffer(): void
    {
        $list = json_decode($this->apiClientService->fetch('/offerList', ['offerType' => 'GAS']), true);
        // have 2 gas offers
        $this->assertEquals(2, count($list));
        $this->assertEquals(
            [
                'offerType' => 'GAS',
                'offerName' => 'EKWAG2000',
                'offerDescription' => 'Une offre incroyable',
                'validPromoCodeList' => ['EKWA_WELCOME', 'ALL_2000']
            ],
            $list[0]
        );
        $this->assertEquals(
            [
                'offerType' => 'GAS',
                'offerName' => 'EKWAG3000',
                'offerDescription' => 'Une offre croustillante',
                'validPromoCodeList' => ['EKWA_WELCOME', 'GAZZZZZZZZY']
            ],
            $list[1]
        );
    }
}
